import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { User } from 'src/models/user.interface';
import { UsersService } from 'src/services/users/users.service';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}
  @Get()
  getAllUsers() {
    return this.usersService.getAllUsers();
  }
  @Get(':id')
  getUserById(@Param() id: number) {
    return this.usersService.getUserById(id);
  }
  @Post('login')
  login(@Body() obj: User) {
    return this.usersService.login(obj);
  }
  @Post()
  createUser(@Body() obj: User) {
    return this.usersService.createUser(obj);
  }

  @Delete(':id')
  deleteUser(@Param() id: number) {
    return this.usersService.deleteUser(id);
  }
  @Put()
  updateUser(@Body() obj: User) {
    return this.usersService.updateUser(obj);
  }
}
