import { Injectable } from '@nestjs/common';
import { User } from 'src/models/user.interface';

@Injectable()
export class UsersService {
  arr: User[];
  constructor() {
    this.arr = [
      {
        id: 1,
        name: 'Juan',
        email: 'juan@gmail.com',
        password: '12345',
      },
    ];
  }
  getAllUsers(): User[] {
    return this.arr;
  }
  getUserById(id: number): User {
    return this.arr.find((item: User) => item.id == id);
  }
  login(obj: User): User {
    return this.arr.find((item: User) => item.email == obj.email && item.password == obj.password);
  }
  createUser(obj: User): User {
    obj.id = this.arr.length + 1;
    this.arr.push(obj);
    return obj;
  }
  deleteUser(id: number) {
    this.arr = this.arr.filter((item: User) => item.id != id);
    return this.arr;
  }
  updateUser(obj: User) {
    const user = this.arr.find((item: User) => item.id === obj.id);
    user.email = obj.email;
    user.name = obj.name;
    user.password = obj.password;

    this.arr = this.arr.filter((item: User) => item.id != obj.id);
    this.arr.push(user);

    return user;
  }
}
